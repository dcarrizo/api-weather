 Weather API
========================

## Requirements
If you choose the installation with Docker you are going to need docker and docker composer installed on your machine.
But if you choose to install it manually, you are going to need npm and node installed.

## Installation with docker
Clone the project, create the .env file from the env example and build/run container.

```bash
$ git clone https://gitlab.com/dcarrizo/weather-api.git
$ cd weather-api
$ cp .env.example .env
$ docker-compose up -d
```

And now you can start using the app.

## Installation manually
Clone the project, create the .env file from the env example and install dependencies.

```bash
$ git clone https://gitlab.com/dcarrizo/weather-api.git
$ cd weather-api
$ cp .env.example .env
$ npm i
```

And to start using the app just type:

```bash
$ npm start
```

## Usage
Now that you have the app up and running, you can start using it by entering to the documentation page.
If you choose the Docker installation, the host that you will need to use is  `0.0.0.0:3000`. So to enter to the documentation page you will need to put in the browser `http://0.0.0.0:3000/documentation`. But if you installed it manually you can enter typing  `http://localhost:3000/documentation`

Or you can use postman or curl to make the desired request.
In this examples I will be using curl to send requests from a manually installed app:

1. Get the location:

    ```bash
    $ curl --request GET 'http://localhost:3000/v1/location'
    ```

2. Get the current weather for an specific location:

    ```bash
    $ curl --request GET 'http://localhost:3000/v1/current/london'
    ```

3. Get the forecast weather for an specific location:

    ```bash
    $ curl --request GET 'http://localhost:3000/v1/forecast/london'
    ```

> Note: If you used Docker to install the app remember to change `http://localhost:3000` to `http://0.0.0.0:3000`

