require('dotenv').config();
const fastify = require('fastify');
const oas = require('fastify-oas');
const docs = require('./src/routes/documentation');
const routes = require('./src/routes');

const app = fastify({ logger: true });

app.register(oas, docs);

app.register((instance, opts, next) => {
  routes.weatherRoutes(instance);
  next();
}, { prefix: 'v1' });

const start = async () => {
  try {
    await app.listen(3000, process.env.HOST_INTERFACE || '127.0.0.1');
    app.log.info(`Server listening on ${app.server.address().port}`);
  } catch (error) {
    app.log.error(error);
    process.exit(1);
  }
};

start();
