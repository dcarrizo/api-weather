# Dockerfile
# from base image node
FROM node:12.19-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Node Dependencies
COPY ./package.json /usr/src/app/package.json
COPY ./package-lock.json /usr/src/app/package-lock.json

# Install node packages
RUN npm install

# Copy source code
COPY . /usr/src/app/

EXPOSE 3000

# Command to run when initiate an image
CMD [ "node", "/usr/src/app/index.js" ]
