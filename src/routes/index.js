const locationSchema = require('./schemas/geolocation');
const weatherSchemas = require('./schemas/weather');
const geolocationService = require('../services/geolocation');
const weatherService = require('../services/weather');

const weatherRoutes = async (app) => {
  app.get('/location', { schema: locationSchema }, async (request, reply) => {
    const response = await geolocationService.getCityLocation();
    if (response.error) {
      reply.code(400).send({ message: response.message });
    }
    reply.send({ city: response.city });
  });

  app.get('/current', { schema: weatherSchemas }, async (request, reply) => {
    const response = await weatherService.checkCityAndGetWeather(request.query.city, 'weather');
    if (!response) {
      reply.code(400).send({ message: 'The city is not correct' });
    }
    return reply.type('application/json').send(response);
  });

  app.get('/forecast', { schema: weatherSchemas }, async (request, reply) => {
    const response = await weatherService.checkCityAndGetWeather(request.query.city, 'forecast');
    if (!response) {
      reply.code(400).send({ message: 'The city is not correct' });
    }
    return reply.type('application/json').send(response);
  });
};

module.exports = {
  weatherRoutes,
};
