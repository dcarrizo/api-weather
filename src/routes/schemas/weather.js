module.exports = {
  sumary: 'Weather',
  description: 'Get the current weather',
  querystring: {
    type: 'object',
    properties: {
      city: {
        type: 'string',
      },
    },
    example: {
      city: 'london',
    },
  },
  response: {
    200: {
      description: 'Success',
      type: 'object',
      properties: {
        temp: {
          type: 'string',
          description: 'Weather in degrees string format',
        },
      },
      example: {
        temp: '20.17',
      },
    },
    400: {
      description: 'The city passed is not correct',
      type: 'object',
      properties: {
        message: { type: 'string', description: 'Error response message' },
      },
    },
  },
};
