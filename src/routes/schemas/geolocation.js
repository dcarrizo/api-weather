module.exports = {
  response: {
    200: {
      description: 'Success',
      type: 'object',
      properties: {
        city: { type: 'string' },
      },
    },
    400: {
      description: 'Error type',
      properties: {
        message: { type: 'string' },
      },
    },
  },
};
