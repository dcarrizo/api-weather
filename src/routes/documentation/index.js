const server = process.env.HOST_INTERFACE ? `http://${process.env.HOST_INTERFACE}:3000` : 'http://127.0.0.1:3000';

module.exports = {
  routePrefix: '/documentation',
  swagger: {
    info: {
      title: 'Test weather api',
      description: 'Weather api using fastify',
      version: '1.0.0',
    },
    servers: [{
      url: server,
    }],
    externalDocs: {
      url: 'https://swagger.io',
      description: 'Find more info here',
    },
    consumes: ['application/json'],
    produces: ['application/json'],
  },
  exposeRoute: true,
};
