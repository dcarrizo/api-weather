const fetch = require('node-fetch');
const locationService = require('./geolocation');

const weatherUrl = process.env.OPENWEATHER_URL;
const weatherApiKey = process.env.OPENWEATHER_API_KEY;

const getWeather = async (city, type) => {
  try {
    const paramsUrl = city.city ? `lat=${city.lat}&lon=${city.lon}` : `q=${city}`;
    const fullResponse = await fetch(
      `${weatherUrl}/${type}/?${paramsUrl}&units=metric&appid=${weatherApiKey}`,
    );
    const res = await fullResponse.json();
    let temp;
    if (type === 'forecast') {
      temp = res.list[res.list.length - 1].main.temp;
    } else {
      temp = res.main.temp;
    }
    return { temp };
  } catch (error) {
    return false;
  }
};

const checkCityAndGetWeather = async (cityParam, type) => {
  let city = cityParam;
  if (!city) {
    city = await locationService.getCityLocation();
  }
  return getWeather(city, type);
};

module.exports = {
  getWeather,
  checkCityAndGetWeather,
};
