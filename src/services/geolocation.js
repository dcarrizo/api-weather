const fetch = require('node-fetch');

const ipapiUrl = process.env.IPAPI_URL;
const options = {
  method: 'GET',
  headers: { 'user-agent': 'ipapi/ipapi-nodejs/0.3' },
};

const setHeaders = (header) => {
  options.headers = header;
};

const getCityLocation = async () => {
  try {
    const fullResponse = await fetch(`${ipapiUrl}/json`, options);
    const result = await fullResponse.json();
    if (!result.city) {
      throw new Error(`${result.reason}: ${result.message}`);
    }
    return { city: result.city, lat: result.latitude, lon: result.longitude };
  } catch (error) {
    return { error: true, message: error.message };
  }
};

module.exports = {
  getCityLocation,
  setHeaders,
};
