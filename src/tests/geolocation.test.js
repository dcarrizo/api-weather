const geolocationService = require('../services/geolocation');

describe('Geolocation Service', () => {
  test('getCityLocation returns object containing object city information', async () => {
    const data = await geolocationService.getCityLocation();
    expect(data).toBeTruthy();
    expect(typeof data).toBe('object');
    expect(data).toHaveProperty('lat');
    expect(data).toHaveProperty('lon');
  });

  test('getCityLocation returns error object if there is an error when finding the city', async () => {
    try {
      geolocationService.setHeaders();
      await await geolocationService.getCityLocation();
    } catch (error) {
      expect(typeof error).toBe('object');
      expect(error).toHaveProperty('error');
      expect(error).toHaveProperty('message');
      expect(error.error).toBeFalsy();
    }
  });
});
