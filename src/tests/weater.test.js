const weatherService = require('../services/weather');

describe('Weater Service', () => {
  test('getWeather returns a string when the city is correct for the current weather', async () => {
    const data = await weatherService.getWeather('Buenos Aires', 'weather');
    expect(data).toBeTruthy();
    expect(typeof data).toBe('object');
    expect(data).toHaveProperty('temp');
  });

  test('getWeather return false when the city is not correct', async () => {
    try {
      await weatherService.getWeather('Wrong City', 'weather');
    } catch (error) {
      expect(error).toBeFalsy();
    }
  });

  test('checkCityAndGetWeather returns a string when not passing a city', async () => {
    const data = await weatherService.checkCityAndGetWeather('', 'weather');
    expect(data).toBeTruthy();
    expect(typeof data).toBe('object');
    expect(data).toHaveProperty('temp');
  });

  test('checkCityAndGetWeather return false when the city is not correct', async () => {
    try {
      await weatherService.checkCityAndGetWeather('Wrong City', 'weather');
    } catch (error) {
      expect(error).toBeFalsy();
    }
  });

  test('getWeather returns a string when the city is correct for the forecast weather', async () => {
    const data = await weatherService.getWeather('Buenos Aires', 'forecast');
    expect(data).toBeTruthy();
    expect(typeof data).toBe('object');
    expect(data).toHaveProperty('temp');
  });
});
